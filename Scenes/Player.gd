extends Area2D

var screensize
export var speed = 300

onready var bullet = preload("res://Scenes/Laser.tscn")

func _ready():
	screensize = get_viewport_rect().size

func _process(delta):
	var x_velocity = 0
	var y_velocity = 0
	if Input.is_action_pressed("ui_right"):
		x_velocity = 1
	if Input.is_action_pressed("ui_left"):
		x_velocity = -1
	if Input.is_action_pressed("ui_up"):
		y_velocity = -1
	if Input.is_action_pressed("ui_down"):
		y_velocity = 1

	position.x += x_velocity * speed * delta
	position.x = clamp(position.x, 0, screensize.x)
	position.y += y_velocity * speed * delta
	position.y = clamp(position.y, 0, screensize.y)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)
